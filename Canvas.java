import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JPanel;
import java.awt.Dimension;

public class Canvas extends JPanel {

    private boolean startedSearch = false;
    private int WIDTH;

    public Canvas(int width) {
        WIDTH = width;
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(WIDTH, WIDTH);
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.setColor(Color.BLACK);
        g.fillRect(0, 0, WIDTH, WIDTH);
        if (!startedSearch) {
            Stars.startSearch();
            startedSearch = true;
        }
        Stars.draw(g);
    }
}