import javax.swing.JFrame;

public class Window {

    public Window(int width) {
        JFrame frame = new JFrame("A* Galaxy Search");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.add(new Canvas(width));
        frame.pack();
        frame.setVisible(true);
    }
}
